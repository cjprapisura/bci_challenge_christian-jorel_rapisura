"""
Difficulty: easy
"""
import csv
import json
import time
from selenium import webdriver
from bs4 import BeautifulSoup
from requests.api import options

url = 'https://www.emservices.com.sg/tenders/'
option = webdriver.ChromeOptions()
driver = webdriver.Chrome(executable_path=r"D:\Web Scrapping\chromedriver_win32\chromedriver.exe",options=option)
driver.get(url)
driver.maximize_window()



soup = BeautifulSoup(driver.page_source, 'lxml')
pageLength = driver.find_element_by_xpath('//label/select/option[4]').click() #Select 100 as option since the data needed to be extracted is at range of 46-56
time.sleep(5) #time delay in order for the page to load the list 

advertisement_dates = driver.find_elements_by_xpath('//tbody/tr/td[1]')[45:55] #locate the Advertisement dates in the page
closing_dates = driver.find_elements_by_xpath('//tbody/tr/td[2]')[45:55]#locate the Closing dates
clients = driver.find_elements_by_xpath('//tbody/tr/td[3]')[45:55]#locate the Client's name 
project_titles = driver.find_elements_by_xpath('//tbody/tr/td[4]/a')[45:55]#locate the Project Titles
eligibilities_and_financial_grades = driver.find_elements_by_xpath('//tbody/tr/td[5]')[45:55]#locate the Eligibilities and Financial Grade
download_request_form = driver.find_elements_by_xpath('//tbody/tr/td[6]/a')[45:55]#locate the downloadable links 



file = open("challege_1.csv", 'w', newline='')
writer = csv.writer(file)

#writes header rows
writer.writerow(["Advertisement Date", "Closing Date", "Client", "Description","Eligibility", "Link"])

# acts as storage for the the extracted data
informations = [] 

#traverse each table's data and store the data to the informations' list
for i in range(len(advertisement_dates)):
    temporary_infos = {"Advertisement Date": advertisement_dates[i].text,
                        "Closing Date" : closing_dates[i].text,
                        "Client" : clients[i].text,
                        "Description" : project_titles[i].text,
                        "Eligibility" : eligibilities_and_financial_grades[i].text,
                        "Link" : download_request_form[i].get_attribute('href')
    }
    informations.append(temporary_infos)

file = open('challenge_1.json', mode='w', encoding='utf-8') 
file.write(json.dumps(informations))

for info in informations:
    writer.writerow(info.values())

file.close()

