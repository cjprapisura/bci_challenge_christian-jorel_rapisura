import csv
import json
import time
from typing import Text
from requests.sessions import merge_setting
from selenium import webdriver
from bs4 import BeautifulSoup
from requests.api import get, options

url = 'https://businesspapers.parracity.nsw.gov.au/'
option = webdriver.ChromeOptions()
driver = webdriver.Chrome(executable_path=r"D:\Web Scrapping\chromedriver_win32\chromedriver.exe",options=option)
driver.get(url)
time.sleep(2)


soup = BeautifulSoup(driver.page_source, 'lxml')



meetings_table = soup.find('table', {'id' : 'grdMenu'})
tbody = meetings_table.find('tbody')

file = open("challege_2.csv", 'w', newline='')
writer = csv.writer(file)

#writes header rows
writer.writerow(["meeting_date", "meeting_description", "agenda_html_link", "agenda_pdf_link",'minutes'])
# acts as storage for the the extracted data
informations = [] 

#traverse each table's data and store the data to the informations' list which start at Table Row #8
for tr in tbody.find_all('tr')[7:]: 
    meeting_dates = tr.find_all('td')[0].text.strip()
    meeting_descriptions = tr.find_all('td')[1].text.strip()
    agenda_html_links = tr.find_all('td')[2].find_all('a')[0].attrs['href']
    for hr_links in tr:  
        agenda_pdf_links = tr.find_all('td')[2].find('a').find_next_siblings('a')# can't get the href element of the 2nd 'a'
        if agenda_pdf_links == None:
            continue
    #minutes = tr.find_all('td')[4].find_all('a')[0].attrs['href']
    temporary_infos = {"meeting_date": meeting_dates,
                        "meeting_description" : meeting_descriptions,
                        "agenda_html_link" : agenda_html_links
                        #"agenda_pdf_link" : agenda_pdf_links
     }
    informations.append(temporary_infos)
    print(agenda_pdf_links)

file = open('challenge_2.json', mode='w', encoding='utf-8') 
file.write(json.dumps(informations))

for info in informations:
    writer.writerow(info.values())

file.close()


 



